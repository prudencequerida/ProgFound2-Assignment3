package javari.animal;

public class Mammal extends Animal {

    protected String specialStatus;

    public Mammal(String[] info) {
        super(Integer.parseInt(info[0]), info[1], info[2], Gender.parseGender(info[3]), Double.parseDouble(info[4]),
                Double.parseDouble(info[5]), Condition.parseCondition(info[7]));
        specialStatus = info[6];
    }

    @Override
    protected boolean specificCondition() {
        return !specialStatus.equals("pregnant") && ((!getType().equals("Lion")) || (getGender().equals(Gender.MALE)));
    }
}
