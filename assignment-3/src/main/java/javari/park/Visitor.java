package javari.park;

import java.util.ArrayList;
import java.util.List;

public class Visitor implements Registration {

    static ArrayList<Registration> visitors = new ArrayList<>();
    static int idCounter = 0;

    String name;
    int registrationId;
    List<SelectedAttraction> attractions = new ArrayList<>();

    public Visitor(String name) {
        this.name = name;
        registrationId = ++idCounter;
        visitors.add(this);
    }

    public static Registration findVisitor(String name) {
        for (Registration visitor : visitors) {
            if (visitor.getVisitorName().equalsIgnoreCase(name))
                return visitor;
        }
        return null;
    }

    public static ArrayList<Registration> getVisitors() {
        return visitors;
    }

    @Override
    public String getVisitorName() {
        return name;
    }

    @Override
    public String setVisitorName(String name) {
        this.name = name;
        return name;
    }

    @Override
    public int getRegistrationId() {
        return registrationId;
    }

    @Override
    public List<SelectedAttraction> getSelectedAttractions() {
        return attractions;
    }

    @Override
    public boolean addSelectedAttraction(SelectedAttraction selected) {
        attractions.add(selected);
        return true;
    }
}
