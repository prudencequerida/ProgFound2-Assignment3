package javari;

import javari.animal.Animal;
import javari.park.Attraction;
import javari.park.Registration;
import javari.park.SelectedAttraction;
import javari.park.Visitor;
import javari.reader.AttractionsReader;
import javari.reader.CategoriesReader;
import javari.reader.CsvReader;
import javari.reader.RecordsReader;
import javari.writer.RegistrationWriter;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Office {

    static Scanner s = new Scanner(System.in);

    public static void main(String[] args) {

        System.out.println("Welcome to Javari Park Festival - Registration Service!\n");
        System.out.print("... Opening default section database from data. ");

        CsvReader[] CsvFiles = new CsvReader[3];
        String textPath = System.getProperty("user.dir") + "\\src\\main\\java\\javari\\input";
        while (true) {
            try {
                CsvFiles[0] = new CategoriesReader(Paths.get(textPath, "animals_categories.csv"));
                CsvFiles[1] = new AttractionsReader(Paths.get(textPath, "animals_attractions.csv"));
                CsvFiles[2] = new RecordsReader(Paths.get(textPath, "animals_records.csv"));
                System.out.println("\n... Loading... Success... System is populating data...\n");
                break;
            } catch (IOException e) {
                System.out.println("... File not found or incorrect file!\n");
                System.out.print("Please provide the source data path: ");
                textPath = s.nextLine();
                textPath = textPath.replace("\\", "\\\\");
            }
        }

        System.out.printf("Found %d valid sections and %d invalid sections%n",
                CsvFiles[0].countValidRecords(), CsvFiles[0].countInvalidRecords());
        System.out.printf("Found %d valid attractions and %d invalid attractions%n",
                CsvFiles[1].countValidRecords(), CsvFiles[1].countInvalidRecords());
        System.out.printf("Found %d valid animal categories and %d invalid animal categories%n",
                CsvFiles[0].countValidRecords(), CsvFiles[0].countInvalidRecords());
        System.out.printf("Found %d valid animal records and %d invalid animal records%n",
                CsvFiles[2].countValidRecords(), CsvFiles[2].countInvalidRecords());

        RegistrationService();
    }

    public static void RegistrationService() {

        System.out.println("Welcome to Javari Park Festival - Registration Service!\n");
        System.out.println("Please answer the questions by typing the number. Type # if you want to return to the previous menu");

        if (sectionCheck())
            System.out.println("Bye Fren");

    }

    public static boolean sectionCheck() {
        String input;

        while (true) {
            ArrayList<String> tem = CategoriesReader.getSectionAvailable();
            System.out.println("\nJavari Park has " + tem.size() + " sections:");

            int i = 0;
            for (String section : tem) {
                System.out.println(++i + ". " + section);
            }

            System.out.print("Please choose your preferred section (type the number): ");

            input = s.nextLine();
            if (input.equals("#")) return true;

            try {
                int inp = Integer.parseInt(input);
                if (inp > 0 && inp <= i)
                    if (!animalCheck(tem.get(inp - 1)))
                        return false;
            } catch (NumberFormatException e) {
                System.out.println("You do nothing...");
            }
        }
    }

    public static boolean animalCheck(String section) {
        String input;

        while (true) {
            System.out.println("\n--" + section + "--");
            int i = 0;
            List<String> tem = CsvReader.getAnimalMapping().get(section);
            for (String animal : tem) {
                System.out.println(++i + ". " + animal);
            }

            System.out.print("Please choose your preferred animals (type the number): ");

            input = s.nextLine();
            if (input.equals("#")) return true;

            try {
                int inp = Integer.parseInt(input);
                if (inp > 0 && inp <= i)
                    if (!attractionCheck(tem.get(inp - 1)))
                        return false;
            } catch (NumberFormatException e) {
                System.out.println("You do nothing...");
            }
        }
    }

    public static boolean attractionCheck(String animal) {
        String input;

        ArrayList<Attraction> attractions = Attraction.findAttractions(animal);

        boolean isEmpty = true;

        for (Attraction attraction : attractions) {
            if (!attraction.getPerformers().isEmpty()) {
                isEmpty = false;
                break;
            }
        }

        if (isEmpty) {
            System.out.println("\nUnfortunately, no " + animal + " can perform any attraction, please choose other animals");
            return true;
        }

        while (true) {
            System.out.println("\n--" + animal + "--");
            int i = 0;
            ArrayList<String> tem = new ArrayList<>();
            for (SelectedAttraction attraction : attractions) {
                System.out.println(++i + ". " + attraction.getName());
                tem.add(attraction.getName());
            }

            System.out.print("Please choose your preferred attractions (type the number): ");

            input = s.nextLine();
            if (input.equals("#")) return true;

            try {
                int inp = Integer.parseInt(input);
                if (inp > 0 && inp <= i)
                    if (!lastCheck(attractions.get(inp - 1)))
                        return false;
                    else
                        System.out.println("You do nothing...");
            } catch (NumberFormatException e) {
                System.out.println("You do nothing...");
            }
        }
    }

    public static boolean lastCheck(SelectedAttraction attraction) {

        System.out.println("\nWow, one more step,");
        System.out.print("please let us know your name: ");
        String name = s.nextLine();

        Registration visitor = Visitor.findVisitor(name);

        if (visitor == null) {
            visitor = new Visitor(name);
        }

        System.out.println("\nYeay, final check!");
        System.out.println("Here is your data, and the attraction you chose:");
        System.out.println("Name: " + visitor.getVisitorName());
        System.out.println("Attractions: " + attraction.getName() + " -> " + attraction.getType());
        System.out.print("With:");

        String output = "";

        for (Animal animal : attraction.getPerformers()) {
            output += " " + animal.getName() + ",";
        }

        System.out.println(output.substring(0, output.length() - 1));

        System.out.print("\nIs the data correct? (Y/N): ");

        if (s.nextLine().equalsIgnoreCase("Y")) {
            visitor.addSelectedAttraction(attraction);

            System.out.print("\nThank you for your interest. Would you like to register to other attractions? (Y/N): ");

            if (s.nextLine().equalsIgnoreCase("Y")) {
                return true;
            } else {

                String textPath = System.getProperty("user.dir") + "\\src\\main\\java\\javari\\json";
                for (Registration visitorRn : Visitor.getVisitors()) {
                    try {
                        RegistrationWriter.writeJson(visitorRn, Paths.get(textPath));
                    } catch (IOException e) {
                        System.out.println("Something went wrong with " + visitorRn.getVisitorName());
                    }
                }
                return false;
            }
        } else
            return true;

    }
}