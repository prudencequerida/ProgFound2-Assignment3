package javari.reader;

import java.io.IOException;
import java.nio.file.Path;
import java.util.LinkedHashSet;
import java.util.Set;

public class AttractionsReader extends CsvReader {

    public AttractionsReader(Path file) throws IOException {
        super(file);
    }

    public long countValidRecords() {
        Set<String> valid = new LinkedHashSet<>();

        for (String line : lines) {
            String[] info = line.split(",");
            boolean check = ((attractionsMapping.containsKey(info[1])) ?
                    attractionsMapping.get(info[1]).contains(info[0]) : false);
            if (check)
                valid.add(info[1]);
        }
        return (long) valid.size();
    }

    public long countInvalidRecords() {
        long invalid = 0;

        for (String line : lines) {
            String[] info = line.split(",");
            boolean check = ((attractionsMapping.containsKey(info[1])) ?
                    attractionsMapping.get(info[1]).contains(info[0]) : false);
            if (!check)
                invalid++;
        }
        return invalid;
    }
}
