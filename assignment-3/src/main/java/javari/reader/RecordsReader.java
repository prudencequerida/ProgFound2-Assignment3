package javari.reader;

import javari.animal.Animal;
import javari.animal.Aves;
import javari.animal.Mammal;
import javari.animal.Reptile;
import javari.park.Attraction;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;

public class RecordsReader extends CsvReader {

    private static long valid, total;

    public RecordsReader(Path file) throws IOException {
        super(file);
    }

    public long countValidRecords() {
        valid = 0;
        total = 0;

        for (String line : lines) {
            String[] info = line.split(",");
            Animal tem;
            total++;

            if (validCategory.get("mammals").contains(info[1])) tem = new Mammal(info);
            else if (validCategory.get("aves").contains(info[1])) tem = new Aves(info);
            else if (validCategory.get("reptiles").contains(info[1])) tem = new Reptile(info);
            else continue;

            registerAnimal(tem, info);
            valid++;
        }
        return valid;
    }

    public long countInvalidRecords() {
        return total - valid;
    }

    public void registerAnimal(Animal animal, String[] info) {
        for (Map.Entry<String, List<String>> entry : attractionsMapping.entrySet()) {
            if (entry.getValue().contains(info[1])) {
                Attraction attraction = Attraction.findAttraction(entry.getKey(), info[1]);
                if (attraction == null)
                    attraction = new Attraction(entry.getKey(), info[1]);
                attraction.addPerformer(animal);
            }
        }
    }
}
