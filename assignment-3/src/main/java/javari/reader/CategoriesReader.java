package javari.reader;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.Set;

public class CategoriesReader extends CsvReader {

    private static int checkInt = 0;
    private static ArrayList<String> sectionAvailable;

    public CategoriesReader(Path file) throws IOException {
        super(file);
    }

    public static ArrayList<String> getSectionAvailable() {
        return sectionAvailable;
    }

    public long countValidRecords() {
        Set<String> valid = new LinkedHashSet<>();

        for (String line : lines) {
            String[] info = line.split(",");
            boolean check = false;
            if (validCategory.containsKey(info[1]))
                check = validCategory.get(info[1]).contains(info[0]);
            check &= ((checkInt != 0) || animalMapping.containsKey(info[2]));
            if (check)
                valid.add(info[2]);
        }

        sectionAvailable = (checkInt++ == 0) ? new ArrayList<>(valid) : sectionAvailable;

        return (long) valid.size();
    }

    public long countInvalidRecords() {
        long invalid = 0;

        for (String line : lines) {
            String[] info = line.split(",");
            boolean check = false;
            if (validCategory.containsKey(info[1]))
                check = validCategory.get(info[1]).contains(info[0]);
            check &= ((checkInt != 0) || animalMapping.containsKey(info[2]));
            if (!check)
                invalid++;
        }
        return invalid;
    }
}
